package cpf_test

import (
	"errors"
	"testing"

	"gitlab.com/llentini/movies-ticket-service/domain/value_object/cpf"
)

func TestNewCPF(t *testing.T) {
	testCases := []struct {
		testName    string
		cpfInput    string
		expectedCpf string
		expectedErr error
	}{
		{"Deve criar um CPF válido com sucesso", "73937355049", "739.373.550-49", nil},
		{"Deve retornar erro com um CPF inválido", "00000000000", "", errors.New("Invalid CPF")},
		{"Deve retornar erro com um CPF inválido", "73937355111", "", errors.New("Invalid CPF")},
	}

	for _, tc := range testCases {
		t.Run(tc.testName, func(t *testing.T) {
			cpfOutput, err := cpf.NewCPF(tc.cpfInput)
			if err != nil && err.Error() != tc.expectedErr.Error() {
				t.Errorf("Invalid expected error. Expected: %v, Got: %v", tc.expectedErr, err)
			}

			if cpfOutput != nil {
				if cpfOutput.GetValue() != tc.expectedCpf {
					t.Errorf("Invalid expected CPF. Expected: %s, Got: %s", cpfOutput.GetValue(), tc.expectedCpf)
				}
			}
		})
	}
}
