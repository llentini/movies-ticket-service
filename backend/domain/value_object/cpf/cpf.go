package cpf

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type CPF struct {
	value string
}

func NewCPF(value string) (*CPF, error) {
	value = strings.ReplaceAll(value, ".", "")
	value = strings.ReplaceAll(value, "-", "")
	if len(value) != 11 {
		return nil, errors.New("Invalid CPF")
	}
	if value == "00000000000" || value == "11111111111" || value == "22222222222" ||
		value == "33333333333" || value == "44444444444" || value == "55555555555" ||
		value == "66666666666" || value == "77777777777" || value == "88888888888" || value == "99999999999" {
		return nil, errors.New("Invalid CPF")
	}
	sum := 0
	for i := 0; i < 9; i++ {
		digit, _ := strconv.Atoi(string(value[i]))
		sum += digit * (10 - i)
	}
	remainder := sum % 11
	digit1 := 11 - remainder
	if digit1 >= 10 {
		digit1 = 0
	}
	sum = 0
	for i := 0; i < 10; i++ {
		digit, _ := strconv.Atoi(string(value[i]))
		sum += digit * (11 - i)
	}
	remainder = sum % 11
	digit2 := 11 - remainder
	if digit2 >= 10 {
		digit2 = 0
	}
	if digit1 != int(value[9]-'0') || digit2 != int(value[10]-'0') {
		return nil, errors.New("Invalid CPF")
	}
	return &CPF{value: fmt.Sprintf("%s.%s.%s-%s", value[:3], value[3:6], value[6:9], value[9:])}, nil
}

func (cpf *CPF) GetValue() string {
	return cpf.value
}
