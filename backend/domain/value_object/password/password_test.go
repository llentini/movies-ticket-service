package password_test

import (
	"errors"
	"testing"

	"gitlab.com/llentini/movies-ticket-service/domain/value_object/password"
)

func TestNewPassword(t *testing.T) {
	testCases := []struct {
		testName           string
		inputPasswordValue string
		inputPasswordSalt  string
		expectedErr        error
	}{
		{"Deve criar um password com sucesso", "1234567", "", nil},
		{"Deve retornar erro ao tentar criar um password com menos de 6 characteres", "12345", "123", errors.New("password length must be greater than 6")},
	}

	for _, tc := range testCases {
		t.Run(tc.testName, func(t *testing.T) {
			passwordOutput, err := password.NewPassword(tc.inputPasswordValue, tc.inputPasswordSalt)
			if err != nil && err.Error() != tc.expectedErr.Error() {
				t.Errorf("Invalid error. Got: %v, Expected: %v", err, tc.expectedErr)
			}
			if passwordOutput != nil && passwordOutput.GetValue() == "" {
				t.Errorf("Password should not be empty Got: %v, Expected: not empty", passwordOutput.GetValue())
			}
		})
	}
}

func TestIsPasswordEqualToHashed(t *testing.T) {
	fakePassword, _ := password.NewPassword("123456", "fake_salt")
	testCases := []struct {
		testName     string
		input        string
		expectResult bool
	}{
		{"Deve retornar que a senha está correta", "123456", true},
		{"Deve retornar que a senha não está correta", "123413515", false},
	}

	for _, tc := range testCases {
		t.Run(tc.testName, func(t *testing.T) {
			isPasswordEqual, _ := fakePassword.IsPasswordEqualToHashed(tc.input)
			if isPasswordEqual != tc.expectResult {
				t.Errorf("Invalid value for isPasswordEqual. Got: %v, Expected: %v", isPasswordEqual, tc.expectResult)
			}
		})
	}
}
