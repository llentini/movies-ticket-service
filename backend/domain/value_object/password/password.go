package password

import (
	"crypto/rand"
	"encoding/hex"
	"errors"

	"golang.org/x/crypto/bcrypt"
)

type Password struct {
	value string
	salt  string
}

func NewPassword(value string, salt string) (*Password, error) {
	if len(value) < 6 {
		return nil, errors.New("password length must be greater than 6")
	}
	if salt == "" {
		generatedSalt, err := generateSalt()
		if err != nil {
			return nil, err
		}
		salt = generatedSalt
	}
	hashedPassword, err := hashPassword(value, salt)
	if err != nil {
		return nil, err
	}
	return &Password{value: hashedPassword, salt: salt}, nil
}

func (password *Password) IsPasswordEqualToHashed(value string) (bool, error) {
	passwordWithSalt := append([]byte(value), []byte(password.salt)...)
	if err := bcrypt.CompareHashAndPassword([]byte(password.value), passwordWithSalt); err != nil {
		return false, err
	}
	return true, nil
}

func (password *Password) GetValue() string {
	return password.value
}

func (password *Password) GetSaltValue() string {
	return password.salt
}

func generateSalt() (string, error) {
	salt := make([]byte, 16)
	_, err := rand.Read(salt)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(salt), nil
}

func hashPassword(password string, salt string) (string, error) {
	passwordWithSalt := append([]byte(password), salt...)
	hashedPassword, err := bcrypt.GenerateFromPassword(passwordWithSalt, bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	return string(hashedPassword), nil
}
