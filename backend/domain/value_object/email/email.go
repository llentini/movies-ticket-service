package email

import (
	"errors"
	"regexp"
)

type Email struct {
	value string
}

func NewEmail(value string) (*Email, error) {
	emailRegex := `^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$`
	match, _ := regexp.MatchString(emailRegex, value)
	if !match {
		return nil, errors.New("Invalid e-mail")
	}
	return &Email{value: value}, nil
}

func (email *Email) GetValue() string {
	return email.value
}
