package email_test

import (
	"errors"
	"testing"

	"gitlab.com/llentini/movies-ticket-service/domain/value_object/email"
)

func TestNewEmail(t *testing.T) {
	testCases := []struct {
		testName    string
		emailInput  string
		expectedErr error
	}{
		{"Deve criar um e-mail válido com sucesso", "john.doe@gmail.com", nil},
		{"Deve retornar erro ao tentar criar um e-mail inválido", "john.doe", errors.New("Invalid e-mail")},
		{"Deve retornar erro ao tentar criar um e-mail inválido", "john.doeeee.com", errors.New("Invalid e-mail")},
	}
	for _, ts := range testCases {
		t.Run(ts.testName, func(t *testing.T) {
			_, err := email.NewEmail(ts.emailInput)
			if err != nil && err.Error() != ts.expectedErr.Error() {
				t.Errorf("Invalid expected error. Expected: %v, Got: %v", ts.expectedErr, err)
			}
		})
	}
}
