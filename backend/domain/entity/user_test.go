package entity_test

import (
	"errors"
	"testing"

	"gitlab.com/llentini/movies-ticket-service/domain/entity"
	"gitlab.com/llentini/movies-ticket-service/domain/value_object/cpf"
)

func TestNewUser(t *testing.T) {
	validCpf, _ := cpf.NewCPF("739.373.550-49")

	testCases := []struct {
		name               string
		userInput          entity.NewUserDTO
		expectedUserOutput *entity.User
		expectedErr        error
	}{
		{"Deve criar um usuário com sucesso", entity.NewUserDTO{Username: "John Doe", Nickname: "john.doe", Password: "1234567", Document: "739.373.550-49", Email: "john.doe@gmail.com"}, &entity.User{Username: "John Doe", Document: validCpf, IsActive: false}, nil},
		{"Deve retornar erro ao tentar criar um usuário com CPF inválido", entity.NewUserDTO{Username: "John Doe", Nickname: "john.doe", Document: "000.000.000-00", Email: "john.doe@gmail.com"}, nil, errors.New("Invalid CPF")},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			userOutput, err := entity.NewUser(tc.userInput)
			if err != nil && err.Error() != tc.expectedErr.Error() {
				t.Errorf("Invalid expected error. Expected: %v, Got: %v", tc.expectedErr, err)
			}
			if userOutput != nil && (userOutput.Username != tc.expectedUserOutput.Username ||
				userOutput.Document.GetValue() != tc.expectedUserOutput.Document.GetValue() ||
				userOutput.IsActive != tc.expectedUserOutput.IsActive) {
				t.Errorf("Invalid expected user. Expected: %v, Got: %v", tc.expectedUserOutput, userOutput)
			}
		})
	}
}
