package entity

import (
	"errors"
	"time"

	"github.com/google/uuid"
	"gitlab.com/llentini/movies-ticket-service/domain/value_object/cpf"
	"gitlab.com/llentini/movies-ticket-service/domain/value_object/email"
	"gitlab.com/llentini/movies-ticket-service/domain/value_object/password"
)

type User struct {
	ID                      uuid.UUID
	Username                string
	Nickname                string
	Password                *password.Password
	PasswordSalt            string
	Email                   string
	Document                *cpf.CPF
	PhotoURL                string
	CreatedAt               time.Time
	UpdatedAt               time.Time
	IsActive                bool
	QuantityOfMoviesWatched int
	LastMovieWatched        uuid.UUID
}

type NewUserDTO struct {
	Username     string
	Nickname     string
	Email        string
	Document     string
	Password     string
	PasswordSalt string
	PhotoURL     string
}

func NewUser(input NewUserDTO) (*User, error) {
	if input.Username == "" {
		return nil, errors.New("empty username")
	}
	if input.Nickname == "" {
		return nil, errors.New("empty nickname")
	}
	email, err := email.NewEmail(input.Email)
	if err != nil {
		return nil, err
	}
	cpf, err := cpf.NewCPF(input.Document)
	if err != nil {
		return nil, err
	}
	pass, err := password.NewPassword(input.Password, input.PasswordSalt)
	if err != nil {
		return nil, err
	}
	return &User{
		ID:                      uuid.New(),
		Username:                input.Username,
		Nickname:                input.Nickname,
		PhotoURL:                input.PhotoURL,
		Document:                cpf,
		Password:                pass,
		PasswordSalt:            pass.GetSaltValue(),
		Email:                   email.GetValue(),
		IsActive:                false,
		CreatedAt:               time.Now(),
		UpdatedAt:               time.Now(),
		QuantityOfMoviesWatched: 0,
	}, nil
}

func (u *User) ShouldAuthenticateWithPassword(passwordValue string) bool {
	shouldAuth, _ := u.Password.IsPasswordEqualToHashed(passwordValue)
	return shouldAuth
}
