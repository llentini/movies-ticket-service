package repository

import (
	"context"
	"fmt"

	"gitlab.com/llentini/movies-ticket-service/domain/entity"
)

type UserRepositoryInMemory struct {
	users []entity.User
}

func NewUserRepositoryMemory() (*UserRepositoryInMemory, error) {
	return &UserRepositoryInMemory{users: make([]entity.User, 0)}, nil
}

func (r *UserRepositoryInMemory) GetById(ctx context.Context, id string) (*entity.User, error) {
	fmt.Printf("Get By Id: %v \n", &r.users[0])
	return &r.users[0], nil
}

func (r *UserRepositoryInMemory) GetByDocument(ctx context.Context, document string) (*entity.User, error) {
	return &r.users[0], nil
}

func (r *UserRepositoryInMemory) GetByEmail(ctx context.Context, email string) (*entity.User, error) {
	return &r.users[0], nil
}

func (r *UserRepositoryInMemory) Save(ctx context.Context, user *entity.User) error {
	r.users = append(r.users, *user)
	return nil
}
