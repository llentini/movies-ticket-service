package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/llentini/movies-ticket-service/application/provider"
	"gitlab.com/llentini/movies-ticket-service/application/usecase"
)

type HttpHandlerMux struct {
	server           *http.ServeMux
	getUser          *usecase.GetUser
	createUser       *usecase.CreateUser
	authneticateUser *usecase.AuthenticateUser
	tokenProvider    provider.AuthenticateTokenProvider
	host             string
	port             string
}

type CreateUserRequestBody struct {
	Username string `json:"username"`
	Nickname string `json:"nickname"`
	Password string `json:"password"`
	Email    string `json:"email"`
	Document string `json:"document"`
}

type AuthenticateUserRequestBody struct {
	Document string `json:"document"`
	Password string `json:"password"`
}

func NewHttpHandlerMux(
	ctx context.Context,
	server *http.ServeMux,
	tokenProvider provider.AuthenticateTokenProvider,
	getUser *usecase.GetUser,
	createUser *usecase.CreateUser,
	authUser *usecase.AuthenticateUser,
	host string,
	port string,
) (*HttpHandlerMux, error) {
	return &HttpHandlerMux{server: server, tokenProvider: tokenProvider, getUser: getUser, createUser: createUser, authneticateUser: authUser, host: host, port: port}, nil
}

func (s *HttpHandlerMux) CreateRoutes(ctx context.Context) {
	s.server.HandleFunc("GET /v1/user/{id}", s.middlewareValidateToken(ctx, s.handlerGetUser(ctx)))
	s.server.HandleFunc("POST /v1/user", s.handlerCreateUser(ctx))
	s.server.HandleFunc("POST /v1/user/auth", s.handlerAuthenticateUser(ctx))
}

func (s *HttpHandlerMux) Serve(ctx context.Context) error {
	log.Printf("Serving at %s:%s", s.host, s.port)
	return http.ListenAndServe(fmt.Sprintf("%s:%s", s.host, s.port), s.server)
}

func (s *HttpHandlerMux) handlerGetUser(ctx context.Context) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Printf("handling get req at %s\n", r.URL.Path)
		id := r.PathValue("id")
		userOutput, err := s.getUser.Execute(ctx, id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		renderJSON(w, userOutput)
	}
}

func (s *HttpHandlerMux) handlerCreateUser(ctx context.Context) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Printf("handling post req at %s\n", r.URL.Path)
		var createUserRequestBody CreateUserRequestBody
		if err := json.NewDecoder(r.Body).Decode(&createUserRequestBody); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		createUserOutput, err := s.createUser.Execute(ctx, usecase.CreateUserInputDTO{
			Username: createUserRequestBody.Username,
			Nickname: createUserRequestBody.Nickname,
			Password: createUserRequestBody.Password,
			Email:    createUserRequestBody.Email,
			Document: createUserRequestBody.Document,
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		renderJSON(w, createUserOutput.ID)
	}
}

func (s *HttpHandlerMux) handlerAuthenticateUser(ctx context.Context) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Printf("handling post req at %s\n", r.URL.Path)
		var authUserRequestBody AuthenticateUserRequestBody
		if err := json.NewDecoder(r.Body).Decode(&authUserRequestBody); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		authUserOutput, err := s.authneticateUser.Execute(ctx, usecase.AuthenticateUserInputDTO{
			Document: authUserRequestBody.Document,
			Password: authUserRequestBody.Password,
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		renderJSON(w, authUserOutput.Token)
	}
}

func (s *HttpHandlerMux) middlewareValidateToken(ctx context.Context, handlerFunc http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Printf("token middleware validation")
		tokenHeader := r.Header.Get("x-jwt-token")
		if tokenHeader == "" {
			http.Error(w, "missing 'x-jwt-token' on header", http.StatusBadRequest)
			return
		}
		userID, err := s.tokenProvider.Validate(ctx, tokenHeader)
		log.Printf("user id from token: %v", userID)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		_, err = s.getUser.Execute(ctx, userID)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		handlerFunc(w, r)
	}
}

func renderJSON(w http.ResponseWriter, data interface{}) {
	js, err := json.Marshal(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
