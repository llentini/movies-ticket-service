package provider

import (
	"context"
	"errors"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

type TokenProviderJWT struct {
	secretKey []byte
}

func NewTokenProviderJWT(key []byte) (*TokenProviderJWT, error) {
	return &TokenProviderJWT{secretKey: key}, nil
}

func (p *TokenProviderJWT) Generate(ctx context.Context, userID string) (string, error) {
	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": userID,
		"exp": time.Now().Add(time.Second * 24).Unix(),
	})
	tokenString, err := claims.SignedString(p.secretKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func (p *TokenProviderJWT) Validate(ctx context.Context, tokenString string) (string, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("unexpected signing method")
		}
		return p.secretKey, nil
	})
	if err != nil {
		return "", err
	}
	if !token.Valid {
		return "", errors.New("invalid token")
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return "", errors.New("invalid token claims")
	}
	userID, ok := claims["sub"].(string)
	if !ok {
		return "", errors.New("invalid token subject")
	}
	return userID, nil
}
