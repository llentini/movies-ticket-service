package main

import (
	"context"
	"net/http"

	"gitlab.com/llentini/movies-ticket-service/application/usecase"
	"gitlab.com/llentini/movies-ticket-service/infra/handler"
	"gitlab.com/llentini/movies-ticket-service/infra/provider"
	"gitlab.com/llentini/movies-ticket-service/infra/repository"
)

func main() {
	ctx := context.Background()
	userRepo, err := repository.NewUserRepositoryMemory()
	if err != nil {
		panic(err)
	}
	getUserUsecase, err := usecase.NewGetUser(userRepo)
	if err != nil {
		panic(err)
	}
	createUserUsecase, err := usecase.NewCreateUser(userRepo)
	if err != nil {
		panic(err)
	}
	tokenProvider, err := provider.NewTokenProviderJWT([]byte("secret"))
	if err != nil {
		panic(err)
	}
	authenticateUserUsecase, err := usecase.NewAuthenticateUser(ctx, userRepo, tokenProvider)
	serverMux := http.NewServeMux()
	server, err := handler.NewHttpHandlerMux(ctx, serverMux, tokenProvider, getUserUsecase, createUserUsecase, authenticateUserUsecase, "localhost", "8090")
	if err != nil {
		panic(err)
	}
	server.CreateRoutes(ctx)
	server.Serve(ctx)
}
