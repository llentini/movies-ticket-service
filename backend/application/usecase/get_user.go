package usecase

import (
	"context"
	"errors"

	"gitlab.com/llentini/movies-ticket-service/application/repository"
)

type GetUserOutputDTO struct {
	ID       string `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Document string `json:"document"`
}

type GetUser struct {
	userRepository repository.UserRepository
}

func NewGetUser(userRepo repository.UserRepository) (*GetUser, error) {
	return &GetUser{userRepository: userRepo}, nil
}

func (c *GetUser) Execute(ctx context.Context, id string) (*GetUserOutputDTO, error) {
	if id == "" {
		return nil, errors.New("empty id")
	}
	user, err := c.userRepository.GetById(ctx, id)
	if err != nil {
		return nil, err
	}
	return &GetUserOutputDTO{
		ID:       user.ID.String(),
		Username: user.Username,
		Email:    user.Email,
		Document: user.Document.GetValue(),
	}, nil
}
