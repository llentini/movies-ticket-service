package usecase_test

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/llentini/movies-ticket-service/application/usecase"
	"gitlab.com/llentini/movies-ticket-service/domain/entity"
	"gitlab.com/llentini/movies-ticket-service/domain/value_object/cpf"

	mock_repository "gitlab.com/llentini/movies-ticket-service/mocks"
)

type getUserExpectedOutput struct {
	getUserOutput *usecase.GetUserOutputDTO
	err           error
}

func TestGetUserExecute(t *testing.T) {
	ctx := context.Background()
	fakeUserID := uuid.New()
	validCpf, _ := cpf.NewCPF("739.373.550-49")
	fakeUserFromRepo := &entity.User{ID: fakeUserID, Username: "John Doe", Email: "john.doe@gmail.com", Document: validCpf}
	testCases := []struct {
		desc  string
		mocks func(
			userRepo *mock_repository.MockUserRepository,
		)
		input    string
		expected getUserExpectedOutput
	}{
		{
			desc: "Success",
			mocks: func(
				userRepo *mock_repository.MockUserRepository,
			) {
				userRepo.EXPECT().
					GetById(ctx, fakeUserID.String()).
					Times(1).
					Return(fakeUserFromRepo, nil)
			},
			input: fakeUserID.String(),
			expected: getUserExpectedOutput{
				getUserOutput: &usecase.GetUserOutputDTO{
					ID:       fakeUserID.String(),
					Username: fakeUserFromRepo.Username,
					Email:    fakeUserFromRepo.Email,
					Document: fakeUserFromRepo.Document.GetValue(),
				},
				err: nil,
			},
		},
		{
			desc: "Should return error if id is empty",
			mocks: func(
				userRepo *mock_repository.MockUserRepository,
			) {
			},
			input: "",
			expected: getUserExpectedOutput{
				getUserOutput: nil,
				err:           errors.New("empty id"),
			},
		},
		{
			desc: "Should return error if user does not exist",
			mocks: func(
				userRepo *mock_repository.MockUserRepository,
			) {
				userRepo.EXPECT().
					GetById(ctx, fakeUserID.String()).
					Times(1).
					Return(nil, errors.New("user does not exist"))
			},
			input: fakeUserID.String(),
			expected: getUserExpectedOutput{
				getUserOutput: nil,
				err:           errors.New("user does not exist"),
			},
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			t.Parallel()
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			userRepo := mock_repository.NewMockUserRepository(ctrl)
			tc.mocks(userRepo)
			getUserUseCase, _ := usecase.NewGetUser(userRepo)
			userOutput, err := getUserUseCase.Execute(ctx, tc.input)
			assert.Equal(t, tc.expected.err, err, "Error mismatch")
			assert.Equal(t, tc.expected.getUserOutput, userOutput, "UserOutput mismatch")
		})
	}
}
