package usecase

import (
	"context"

	"gitlab.com/llentini/movies-ticket-service/application/repository"
	"gitlab.com/llentini/movies-ticket-service/domain/entity"
)

type CreateUserInputDTO struct {
	Username string
	Nickname string
	Password string
	Email    string
	Document string
}

type CreateUserOutputDTO struct {
	ID string `json:"id"`
}

type CreateUser struct {
	userRepository repository.UserRepository
}

func NewCreateUser(userRepo repository.UserRepository) (*CreateUser, error) {
	return &CreateUser{userRepository: userRepo}, nil
}

func (c *CreateUser) Execute(ctx context.Context, input CreateUserInputDTO) (*CreateUserOutputDTO, error) {
	user, err := entity.NewUser(entity.NewUserDTO{
		Username: input.Username,
		Nickname: input.Nickname,
		Password: input.Password,
		Email:    input.Email,
		Document: input.Document,
	})
	if err != nil {
		return nil, err
	}
	if err := c.userRepository.Save(ctx, user); err != nil {
		return nil, err
	}
	return &CreateUserOutputDTO{ID: user.ID.String()}, nil
}
