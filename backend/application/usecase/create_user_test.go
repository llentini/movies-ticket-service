package usecase_test

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/llentini/movies-ticket-service/application/usecase"
	mock_repository "gitlab.com/llentini/movies-ticket-service/mocks"
)

type createUserExpectedOutput struct {
	createUserOutput *usecase.CreateUserOutputDTO
	err              error
}

func TestCreateUserExecute(t *testing.T) {
	ctx := context.Background()
	fakeUserInput := usecase.CreateUserInputDTO{
		Username: "John Doe",
		Email:    "john.doe@gmail.com",
		Password: "123456496",
		Nickname: "john.doe",
		Document: "739.373.550-49",
	}
	fakeUserFromRepoOutput := uuid.New()
	testCases := []struct {
		desc  string
		mocks func(
			userRepo *mock_repository.MockUserRepository,
		)
		input    usecase.CreateUserInputDTO
		expected createUserExpectedOutput
	}{
		{
			desc: "Success",
			mocks: func(
				userRepo *mock_repository.MockUserRepository,
			) {
				userRepo.EXPECT().
					Save(ctx, gomock.Any()).
					Times(1).
					Return(nil)
			},
			input: fakeUserInput,
			expected: createUserExpectedOutput{
				createUserOutput: &usecase.CreateUserOutputDTO{
					ID: fakeUserFromRepoOutput.String(),
				},
				err: nil,
			},
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			t.Parallel()
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			userRepo := mock_repository.NewMockUserRepository(ctrl)
			tc.mocks(userRepo)
			createUserUseCase, _ := usecase.NewCreateUser(userRepo)
			createUserOutput, err := createUserUseCase.Execute(ctx, tc.input)
			assert.Equal(t, tc.expected.err, err, "Error mismatch")
			assert.NotEmpty(t, createUserOutput.ID)
		})
	}
}
