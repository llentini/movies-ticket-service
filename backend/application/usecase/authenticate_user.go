package usecase

import (
	"context"
	"errors"

	"gitlab.com/llentini/movies-ticket-service/application/provider"
	"gitlab.com/llentini/movies-ticket-service/application/repository"
)

type AuthenticateUserInputDTO struct {
	Document string
	Password string
}

type AuthenticateUserOutputDTO struct {
	Token string
}

type AuthenticateUser struct {
	userRepository    repository.UserRepository
	authTokenProvider provider.AuthenticateTokenProvider
}

func NewAuthenticateUser(ctx context.Context, userRepo repository.UserRepository, tokenProvider provider.AuthenticateTokenProvider) (*AuthenticateUser, error) {
	return &AuthenticateUser{userRepository: userRepo, authTokenProvider: tokenProvider}, nil
}

func (uc *AuthenticateUser) Execute(ctx context.Context, input AuthenticateUserInputDTO) (*AuthenticateUserOutputDTO, error) {
	user, err := uc.userRepository.GetByDocument(ctx, input.Document)
	if err != nil {
		return nil, err
	}
	if !user.ShouldAuthenticateWithPassword(input.Password) {
		return nil, errors.New("invalid password")
	}
	token, err := uc.authTokenProvider.Generate(ctx, user.ID.String())
	if err != nil {
		return nil, err
	}
	return &AuthenticateUserOutputDTO{Token: token}, nil
}
