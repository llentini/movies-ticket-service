package usecase_test

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/llentini/movies-ticket-service/application/usecase"
	"gitlab.com/llentini/movies-ticket-service/domain/entity"
	mock_repository "gitlab.com/llentini/movies-ticket-service/mocks"
)

type authenticateUserExpectedOutput struct {
	authenticateUserOutput *usecase.AuthenticateUserOutputDTO
	err                    error
}

func TestAuthenticateUserExecute(t *testing.T) {
	ctx := context.Background()
	fakeAuthenticateUserInput := usecase.AuthenticateUserInputDTO{
		Document: "739.373.550-49",
		Password: "1234567",
	}
	fakeUser, err := entity.NewUser(entity.NewUserDTO{
		Username:     "John Doe",
		Email:        "john.doe@gmail.com",
		Nickname:     "john.doe",
		Password:     "1234567",
		PasswordSalt: "salt",
		Document:     "739.373.550-49",
	})
	if err != nil {
		t.Fatal(err)
	}
	fakeTokenFromProvider := "fakeToken"
	testCases := []struct {
		desc  string
		mocks func(
			userRepo *mock_repository.MockUserRepository,
			tokenProvider *mock_repository.MockAuthenticateTokenProvider,
		)
		input    usecase.AuthenticateUserInputDTO
		expected authenticateUserExpectedOutput
	}{
		{
			desc: "Success",
			mocks: func(
				userRepo *mock_repository.MockUserRepository,
				tokenProvider *mock_repository.MockAuthenticateTokenProvider,
			) {
				userRepo.EXPECT().
					GetByDocument(ctx, fakeAuthenticateUserInput.Document).
					Times(1).
					Return(fakeUser, nil)
				tokenProvider.EXPECT().
					Generate(ctx, fakeUser.ID.String()).
					Times(1).
					Return(fakeTokenFromProvider, nil)
			},
			input: fakeAuthenticateUserInput,
			expected: authenticateUserExpectedOutput{
				authenticateUserOutput: &usecase.AuthenticateUserOutputDTO{
					Token: fakeTokenFromProvider,
				},
				err: nil,
			},
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			t.Parallel()
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			userRepo := mock_repository.NewMockUserRepository(ctrl)
			tokenProvider := mock_repository.NewMockAuthenticateTokenProvider(ctrl)
			tc.mocks(userRepo, tokenProvider)
			authenticateUserUseCase, _ := usecase.NewAuthenticateUser(ctx, userRepo, tokenProvider)
			authenticateUserOutput, err := authenticateUserUseCase.Execute(ctx, tc.input)
			assert.Equal(t, tc.expected.err, err, "Error mismatch")
			assert.Equal(t, authenticateUserOutput.Token, tc.expected.authenticateUserOutput.Token)
		})
	}

}
