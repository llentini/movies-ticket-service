package repository

import (
	"context"

	"gitlab.com/llentini/movies-ticket-service/domain/entity"
)

type UserRepository interface {
	GetById(ctx context.Context, id string) (*entity.User, error)
	GetByDocument(ctx context.Context, document string) (*entity.User, error)
	GetByEmail(ctx context.Context, email string) (*entity.User, error)
	Save(ctx context.Context, user *entity.User) error
}
